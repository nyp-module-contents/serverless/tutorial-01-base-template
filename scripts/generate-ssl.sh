#!/bin/bash
mkdir -p dev-certs
openssl req -x509 -nodes -new -sha256 -days 1024 -newkey rsa:2048 -keyout ./dev-certs/key.pem -out ./dev-certs/cert.pem -subj "/C=US/CN=LocalHost-CA"
openssl x509 -outform pem -in ./dev-certs/cert.pem -out ./dev-certs/root.crt
import type { AWS } from "@serverless/typescript";

module.exports = {
	service: "serverless-example",
	frameworkVersion: "3",
	plugins: [
		"serverless-esbuild",
		"serverless-offline"
	],
	useDotenv: true,
	configValidationMode: "error",
	provider: {
		name:         "aws",
		runtime:      "nodejs16.x",
		region:       "ap-southeast-1",
		architecture: "x86_64",
		memorySize:   256,
		timeout:      8,
		logRetentionInDays: 14,
		versionFunctions:   false,
		apiGateway: {
			minimumCompressionSize: 1024,
			shouldStartNameWithService: true,
		},
		httpApi: {
			name:    "example-services-api",
			payload: '2.0',
			metrics: true,
			cors:    {
				allowCredentials: true,
				allowedOrigins:   ["http://localhost:3000", "https://localhost:3000"],
				allowedHeaders:   ["Authorization", "Content-Type"],
				allowedMethods:   ["GET", "POST", "OPTIONS"],
				exposedResponseHeaders: ["Special-Response-Header"],
			}
		},
		environment: {
			AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
			NODE_OPTIONS: "--enable-source-maps --stack-trace-limit = 1000",
		},
	},
	functions: {
		fHelloWorld: {
			handler: "src/functions/example.simple_handler",
			events: [
				{
					http: {
						method: "get",
						path:   "example/hello",
					}
				}
			]
		},
		fHelloPost: {
			handler: "src/functions/example.simple_post",
			events: [
				{
					http: {
						method: "post",
						path:   "example/hello",
					}
				}
			]
		},
		fHelloPostParam: {
			handler: "src/functions/example.simple_post_param",
			events: [
				{
					http: {
						method: "post",
						path:   "example/hello/{param0}/{param1}",
						request: {
							parameters: {
								paths: {
									"param0": { required: true },
									"param1": { required: true },
								}
							}
						}
					}
				}
			]
		},
		fHelloPostValidate: {
			handler: "src/functions/example.simple_post",
			events: [
				{
					http: {
						method: "post",
						path:   "example/hello/validate",
						request: {
							schemas: {
								"application/json": {
									"$schema": "http://json-schema.org/draft-07/schema#",
									"type": "object",
									"properties": {
										"param1": { "type": "string" },
										"param2": { "type": "string" },
										"param3": { "type": "array", "items": { "type": "string" } }
									},
									"required": ["param1", "param2", "param3"]
								}
							}
						}
					}
				}
			]
		}
	},
	package: {
		individually: true
	},
	custom: {
		"esbuild": {
			bundle: true,
			minify: false,
			sourcemap: true,
			exclude: [
				"aws-sdk"
			],
			target: "node16",
			define: {
				"require.resolve": undefined
			},
			platform: "node",
			concurrency: 10,
		},
		"serverless-offline": {
			httpsProtocol: "dev-certs",
			httpPort: 4000,
			lambdaPort: 3999,
		}
	},
} as AWS;
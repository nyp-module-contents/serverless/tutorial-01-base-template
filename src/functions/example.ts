import { parse_content } from "@libs/gateway";
import { APIGatewayProxyEvent, Context } from "aws-lambda";

/**
 * An example Hello World GET
 * @param event    Incoming request event
 * @param context  Current execution context
 * @returns 
 */
export async function simple_handler(event: APIGatewayProxyEvent, context: Context) {
	return {
		isbase64Encoded: false,
		statusCode: 200,
		body: JSON.stringify({
			message: "Hello World!"
		})
	};
}

/**
 * An example Hello World POST returning exactly what you sent
 *
 * For Validation Schema or You do yourself in the function
 * FAIL CASE: 
 * curl -k -XPOST -H "Content-Type: application/json" -d '{"param1": 1, "param2": "two", "param3": ["1", "2", "3"]}' https://localhost:4000/dev/example/hello/validate
 * PASS CASE:
 * curl -k -X POST -H "Content-Type: application/json" -d '{"param1": "1", "param2": "two", "param3": ["1", "2", "3"]}' https://localhost:4000/dev/example/hello/validate
 *
 * @param event    Incoming request event
 * @param context  Current execution context
 * @returns 
 */
export async function simple_post(event: APIGatewayProxyEvent, context: Context) {
	event = parse_content(event);

	return {
		isbase64Encoded: false,
		statusCode: 200,
		body: JSON.stringify({
			message:   "Hello World!",
			receieved: event.body
		})
	};
}

/**
 * An example Hello World POST returning exactly what you sent
 *
 * @param event    Incoming request event
 * @param context  Current execution context
 * @returns 
 */
export async function simple_post_param(event: APIGatewayProxyEvent, context: Context) {
	event = parse_content(event);
	console.log("Param 0: ", event.pathParameters.param0);
	console.log("Param 1: ", event.pathParameters.param1);
	return {
		isbase64Encoded: false,
		statusCode: 200,
		body: JSON.stringify({
			message:   "Hello World!",
			receieved: event.body,
			parameters: event.pathParameters
		})
	};
}


import { APIGatewayProxyEventBase } from "aws-lambda";

/**
 * Parses the contents of the incoming event and setup defaults. This is done as a type safety
 * in the event that it is not consistently generated with appropriate empty containers.
 * @param event Incoming Http Lambda Event
 * @returns 
 */
export function parse_content<T>(
	event: APIGatewayProxyEventBase<T>) : APIGatewayProxyEventBase<T> {
	if (typeof event.body == "string" || !event.body)
		event.body = JSON.parse(event.body || "{}");
	if (event.queryStringParameters == undefined)
		event.queryStringParameters = {};
	if (event.pathParameters == undefined)
		event.pathParameters = {};
	return event;
} 